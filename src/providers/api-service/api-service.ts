import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiServiceProvider {
  private readonly api_url: string;

  constructor(public http: HttpClient) {
    this.api_url = 'https://boiling-cove-54157.herokuapp.com/api';
  }

  getApiUrl() {
    return this.api_url;
  }

}
