import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { HTTP } from '@ionic-native/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { LandingPage } from '../pages/landing/landing';
import { SignupPage } from '../pages/signup/signup';
import { RegisterPage } from '../pages/register/register';
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { ProviderHomePage } from '../pages/provider-home/provider-home';
import { ApiServiceProvider } from '../providers/api-service/api-service';

@NgModule({
  declarations: [
    MyApp,
    LandingPage,
    SignupPage,
    LoginPage,
    MapPage,
    ProviderHomePage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    BsDropdownModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LandingPage,
    SignupPage,
    LoginPage,
    MapPage,
    ProviderHomePage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HTTP,
    ApiServiceProvider
  ]
})
export class AppModule {}
