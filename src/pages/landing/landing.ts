import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  goToLogin() {
    this.navCtrl.setRoot(LoginPage);
  }

  goToSignup() {
    this.navCtrl.setRoot(SignupPage);
  }

  goToRegister() {
    this.navCtrl.setRoot(SignupPage);
  }

}
