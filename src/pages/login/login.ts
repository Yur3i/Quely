import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

import { MapPage } from '../map/map';
import { ProviderHomePage } from '../provider-home/provider-home';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private user_api_token: string;
  private provider_api_token: string;
  private userLoginUrl: string;
  private providerLoginUrl: string;

  private email: string;
  private password: string;
  private isUser = false;
  private isProvider = false;

  private signUpSuccess = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private apiServiceProvider: ApiServiceProvider
  ) {
    this.userLoginUrl = this.apiServiceProvider.getApiUrl() + '/user/login';
    this.providerLoginUrl = this.apiServiceProvider.getApiUrl() + '/service-provider/login';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    console.log(this.isProvider);
    this.signUpSuccess = this.navParams.get('signUpSuccess');
    
  }

  login() {
    console.log(this.isProvider);
    const data = {
      'email': this.email,
      'password': this.password
    };

    if(this.isUser) {
      this.http.post(this.userLoginUrl, data).subscribe((res: any) => {
        this.user_api_token = res;
      });
      console.log(this.user_api_token);
      this.navCtrl.setRoot(MapPage, {
        'isUser': this.isUser,
        'isProvider': this.isProvider,
        'user_api_token': this.user_api_token
      });
    }

    else if(this.isProvider) {
      this.http.post(this.providerLoginUrl, data).subscribe((res: any) => {
        this.provider_api_token = res;
      });
      console.log(this.provider_api_token);
      this.navCtrl.setRoot(ProviderHomePage, {
        'provider_api_token': this.provider_api_token
      });
    }
      
    
  }

}
