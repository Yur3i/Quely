import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { LoginPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  private first_name: string;
  private last_name: string;
  private email: string;
  private password: string;
  private retyped_password: string;
  private phone_number: number;

  private isUser: boolean;
  private isProvider: boolean;
  private signUpSuccess: boolean;

  private userSignupUrl: string;
  private providerSignupUrl: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private apiServiceProvider: ApiServiceProvider
  ) {
    this.isUser = false;
    this.isProvider = false;
    this.userSignupUrl = this.apiServiceProvider.getApiUrl() + '/user/register';
    this.providerSignupUrl = this.apiServiceProvider.getApiUrl() + '/service-provider/register';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signUp() {
    const postData = {
      'first_name': this.first_name,
      'last_name': this.last_name,
      'email': this.email,
      'password': this.password,
      'retyped_password': this.retyped_password,
      'phone_number': this.phone_number,
    };
    if(this.isUser) {
      this.http.post(this.userSignupUrl, postData).subscribe(() => {
        this.signUpSuccess = true;
      });
      this.navCtrl.setRoot(LoginPage, {
        'signUpSuccess': this.signUpSuccess
      });
    }
    else if(this.isProvider) {
      this.http.post(this.providerSignupUrl, postData).subscribe(() => {
        this.signUpSuccess = true;
      });
      this.navCtrl.setRoot(LoginPage, {
        'signUpSuccess': this.signUpSuccess
      });
    }
  }



}
