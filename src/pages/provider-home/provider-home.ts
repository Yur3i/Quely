import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@Component({
  selector: 'page-provider-home',
  templateUrl: 'provider-home.html',
})
export class ProviderHomePage {
  private readonly providerLogoutUrl: string;
  private provider_api_token: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private apiServiceProvider: ApiServiceProvider
  ) {
    this.providerLogoutUrl = this.apiServiceProvider.getApiUrl() + '/service-provider/logout';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProviderHomePage');
    this.provider_api_token = this.navParams.get('provider_api_token');
  }

  editProfile() {
    console.log('Hello');
  }

  logout() {
    this.http.get(this.providerLogoutUrl, {
      headers: new HttpHeaders().set('x-api-key', this.provider_api_token)
    }).subscribe();
  }
}
