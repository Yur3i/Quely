import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GoogleMap,
  GoogleMaps,
  GoogleMapsEvent,
  GoogleMapOptions,
  Marker
   } from '@ionic-native/google-maps';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
	private isUser: boolean;
	private isProvider: boolean;
	private category: string;
	private categories: any;

  map: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.categories = ['Restaurants', 'Shopping malls', 'Supermarkets/Stores'];
  }

  ionViewDidLoad() {
		this.isUser = this.navParams.get('isUser');
		this.isProvider = this.navParams.get('isProvider');
		// console.log(this.isUser);
		// console.log(this.isProvider);
		
    console.log('ionViewDidLoad MapPage');
    this.loadMap();
  }

  loadMap() {
  	let mapOptions: GoogleMapOptions = {
  		camera: {
  			target: {
  				lat: 4.95893,
  				lng: 8.32695
  			},
  			zoom: 18,
  			tilt: 20
  		},

  		gestures: {
  			scroll: true,
  			zoom: true
  		},

  		controls: {
  			compass: true,
  			myLocationButton: true,
  			zoom: true
  		},
  		mapType: 'MAP_TYPE_ROADMAP'

  	};

  	//create the map
  	this.map = GoogleMaps.create('map', mapOptions);
  	//this.showLoading();

  	//wait for the map to load
  	this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
  		console.log("Map is ready");
  		//this.stopLoading();

  		this.map.addMarker({
  			title: 'SPAR',
  			position: {
  				lat: 4.965345,
  				lng: 8.3300311
  			},
        draggable: true,
  			animation: 'DROP',
  			icon: 'blue',		//'<ion-icon name="cart"></ion-icon>',
  			zIndex: 1

  		}).then((marker: Marker) => {
  			marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
  				//this.map.setCameraZoom(22);
  			});

  		}).catch((err) => {
  			console.log(err);
  		});

  		this.map.addMarker({
  			title: 'Valuemart Supermarket',
  			position: {
  				lat: 4.9765811,
  				lng: 8.336236
  			},
        draggable: true,
  			animation: 'DROP',
  			icon: 'red',
  			zIndex: 2

  		}).then((marker) => {
  			marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
  				//this.map.setCameraZoom(22);
  			});

  		}).catch((err) => {
  			console.log(err);
  		});

  		this.map.addMarker({
  			title: 'Favourite Supermarket',
  			position: {
  				lat: 4.9799833,
  				lng: 8.337988
  			},
        draggable: true,
  			animation: 'DROP',
  			icon: 'red',
  			zIndex: 3

  		}).then((marker: Marker) => {
  			marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
  				//this.map.setCameraZoom(22);
  			});

  		}).catch((err) => {
  			console.log(err);
			});

  	});
  }

}
